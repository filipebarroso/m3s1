import 'package:flutter/material.dart';

Widget wrapDirectionality(Widget widget) {
  return Directionality(
    textDirection: TextDirection.ltr,
    child: widget,
  );
}

Widget wrapMaterial(Widget widget) {
  return MaterialApp(
    home: Scaffold(
      body: Directionality(
        textDirection: TextDirection.ltr,
        child: widget,
      ),
    ),
  );
}
