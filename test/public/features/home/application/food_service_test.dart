import 'package:app_francesinha/public/features/home/application/food_service.dart';
import 'package:app_francesinha/public/features/home/domain/food_item.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('FoodService', () {
    test('Should generate 11 food items', () {
      FoodService foodService = FoodService();

      final result = foodService.generateFoodItems();

      expect(result.first, isA<FoodItem>());
      expect(result.length, 11);
    });
  });
}
